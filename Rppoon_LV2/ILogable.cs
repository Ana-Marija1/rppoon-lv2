﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV2
{
    interface ILogable
    {
        string GetStringRepresentation();
    }
}
