﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 1.
            //Kreirajte objekt klase DiceRoller i u njega ubacite 20 kockica.Baciti sve kockice i ispisati rezultate bacanja
            //kockica na ekran.
            /*
            DiceRoller diceRoller = new DiceRoller();
            const int numberOfDice = 20;

            for(int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();

            IList<int> rollingResults = diceRoller.GetRollingResults();
            foreach(int result in rollingResults)
            {
                Console.WriteLine(result);
            }
            */

            //Zadatak 2.
            //Izmijeniti klasu Die tako da joj se preko konstruktora predaje generator pseudo-slučajnih brojeva.Ponoviti
            //zadatak 1.
            /*
            DiceRoller diceRoller = new DiceRoller();
            Random randomGenerator = new Random();
            const int numberOfDice = 20;

            for (int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(6,randomGenerator));
            }
            diceRoller.RollAllDice();

            IList<int> rollingResults = diceRoller.GetRollingResults();
            foreach (int result in rollingResults)
            {
                Console.WriteLine(result);
            }
            */

            //Zadatak 3.
            //Izmijeniti klasu Die tako da koristi generator pseudo-slučajnih brojeva iz prethodnog primjera
            /*
            DiceRoller diceRoller = new DiceRoller();
            const int numberOfDice = 20;

            for (int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();

            IList<int> rollingResults = diceRoller.GetRollingResults();
            foreach (int result in rollingResults)
            {
                Console.WriteLine(result);
            }
            */

            //Zadatak 4.
            //Napisati klasu ConsoleLogger i klasu FileLogger koje ugrađuju sučelje iz prethodnog primjera. Izmijeniti klasu
            //DiceRoller tako da se može koristiti loggerima koji ugrađuju navedeno sučelje.Ubrizgati ovisnost kroz
            //posebnu metodu.
            /*
            DiceRoller diceRoller = new DiceRoller();
            ILogger logger1=new ConsoleLogger();
            ILogger logger2 = new FileLogger("D:\\fileLogger4");
            const int numberOfDice = 20;

            for (int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();

            diceRoller.SetLogger(logger1);
            diceRoller.LogRollingResults();
            diceRoller.SetLogger(logger2);
            diceRoller.LogRollingResults();
            */

            //Zadatak 5. 
            /*
            DiceRoller diceRoller = new DiceRoller();
            FileLogger fileLogger = new FileLogger("D:\\fileLogger");
            ConsoleLogger consoleLogger = new ConsoleLogger();
            
            const int numberOfDice = 20;

            for (int i = 0; i < numberOfDice; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();

            fileLogger.Log(diceRoller);
            consoleLogger.Log(diceRoller);
            */



            //Zadatak 6.
            /*
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();
            ClosedDiceRoller closedDiceRoller = new ClosedDiceRoller(3,12);

            FileLogger fileLogger1 = new FileLogger("D:\\fileLogger1");
            FileLogger fileLogger2 = new FileLogger("D:\\fileLogger2");
            FileLogger fileLogger3 = new FileLogger("D:\\fileLogger3");
            ConsoleLogger consoleLogger = new ConsoleLogger();

            const int numberOfDice = 20;
            for (int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(12));
            }

            for (int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(6));
            }
            for (int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(20));
            }
            for (int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(8));
            }
            flexibleDiceRoller.RollAllDice();

            fileLogger1.Log(flexibleDiceRoller);
            consoleLogger.Log(flexibleDiceRoller);
            
            flexibleDiceRoller.RemoveAllDice();
            fileLogger2.Log(flexibleDiceRoller);
            consoleLogger.Log(flexibleDiceRoller);

            closedDiceRoller.RollAllDice();
            fileLogger3.Log(closedDiceRoller);
            consoleLogger.Log(closedDiceRoller);
            */

            //Zadatak 7.
            /*
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();

            FileLogger fileLogger = new FileLogger("D:\\fileLogger");
            ConsoleLogger consoleLogger = new ConsoleLogger();

            const int numberOfDice = 20;

            for (int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(12));
            }

            for (int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(6));
            }
            for (int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(20));
            }
            for(int i = 0; i < numberOfDice / 4; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(8));
            }
            flexibleDiceRoller.RemoveDiceByNumberOfSides(6);
            flexibleDiceRoller.RollAllDice();

            fileLogger.Log(flexibleDiceRoller);
            consoleLogger.Log(flexibleDiceRoller);
            flexibleDiceRoller.RemoveAllDice();            
            */
         
        }

    }
}
